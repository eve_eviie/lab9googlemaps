package th.ac.tu.siit.lab9googlemaps;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	GoogleMap map;
	LocationManager locManager;
	Location currentLocation;
	int color = Color.RED;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = (MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true); //display a button to center the map at the current location
		
		MarkerOptions m1 = new MarkerOptions();
		m1.position(new LatLng(0,0));
		map.addMarker(m1);
		
		MarkerOptions m2 = new MarkerOptions();
		m2.position(new LatLng(13.75, 100.4667));
		m2.title("Bangkok");
		map.addMarker(m2);
		
		PolylineOptions p = new PolylineOptions();
		p.add(new LatLng(13.75, 100.4667));
		p.add(new LatLng(35.6895, 139.6917));
		p.width(10);
		p.color(Color.BLUE);
		map.addPolyline(p);
		
		//map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		
		locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		currentLocation = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location l) {
				if(currentLocation != null){
					PolylineOptions p2 = new PolylineOptions();
					p2.add(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
					p2.add(new LatLng(l.getLatitude(),l.getLongitude()));
					p2.width(7);
					p2.color(color);
					map.addPolyline(p2);
				}
				currentLocation = l;
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};
		
		//5000ms = minimum time
		//5 m = minimum distance
		
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, locListener);
	}
	
	Location newLocation;
	
	public boolean onOptionsItemSelected(MenuItem item){
		switch(item.getItemId()) {
			case R.id.mark:
				
				if(currentLocation!=null){
					MarkerOptions m3 = new MarkerOptions();
					m3.position(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
					map.addMarker(m3);
					return true;
				}
				
			case R.id.changecolor:
				color = Color.MAGENTA;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
